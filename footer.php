
  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>ESIGELEC</h3>
            <p>
              Technopôle du Madrillet,<br>
              Av. Galilée, 76800 Saint-Étienne-du-Rouvray<br>
              France <br><br>
              <strong>Téléphone:</strong> +33 XX XX XX XX<br>
              <strong>Email:</strong> info@esigelec.fr<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Liens utiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">Accueil</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about">À propos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#projects">Projets</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#contact">Contacts</a></li>
            </ul>
          </div>

        </div>
      </div>
    </div>

    <div class="container">

      <div class="copyright-wrap d-md-flex py-4">
        <div class="me-md-auto text-center text-md-start">
          <div class="copyright">
            &copy; Copyright <strong><span>Project Manager</span></strong>. All Rights Reserved
          </div>
        </div>
      </div>

    </div>
  </footer><!-- End Footer -->
