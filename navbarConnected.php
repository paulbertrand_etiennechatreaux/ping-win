  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">
      <h1 class="logo"><a href="index.php">Project Manager</a></h1>


<!-- NAVBAR -->
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#counts">Accueil</a></li>
          <li><a class="nav-link scrollto" href="#projects">Projets</a></li>
          <li><a class="getstarted scrollto" href="deconnexionPage.php">Se déconnecter</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
<!-- END NAVBAR -->

    </div>
  </header>
<!-- End Header -->